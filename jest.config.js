module.exports = {
  testEnvironment: "node",
  testRegex: "/.*.test.ts$",
  transform: {
    ".(ts|tsx)": "@sucrase/jest-plugin",
  },
  roots: ["<rootDir>/src/", "<rootDir>/test/"],
  moduleFileExtensions: ["ts", "js"],
  collectCoverageFrom: ["src/**/*.{ts,tsx}", "!**/node_modules/**", "!**/dist/**"],
  watchPlugins: ["jest-watch-typeahead/filename", "jest-watch-typeahead/testname"],
  coverageProvider: "v8",
  reporters: [["default", { test: true }]],
};
