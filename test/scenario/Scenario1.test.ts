/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Game } from "../../src/model/Game";
import { getCompleteScoreText } from "../../src/util/textOutput";
import { AllRoundsPlayedError } from "../../src/error/AllRoundsPlayedError";

describe("Scenario 1", () => {
  it("will play successfully", () => {
    const game = new Game();
    // Round 1
    game.playNextStep(10);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(1);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    expect(game.getScore().getScoreForGame()[0].summedUpScore).toEqual(10);
    // Round 2
    game.playNextStep(9);
    game.playNextStep(1);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(2);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    expect(game.getScore().getScoreForGame()[0].summedUpScore).toEqual(20);
    // Round 3
    game.playNextStep(5);
    game.playNextStep(5);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(3);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    // Round 4
    game.playNextStep(7);
    game.playNextStep(2);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(4);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(9);
    // Round 5
    game.playNextStep(10);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(5);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    // Round 6
    game.playNextStep(10);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(6);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    // Round 7
    game.playNextStep(10);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(7);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    // Round 8
    game.playNextStep(9);
    game.playNextStep(0);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(8);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(9);
    // Round 9
    game.playNextStep(8);
    game.playNextStep(2);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(9);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(10);
    // Round 10
    game.playNextStep(9);
    game.playNextStep(1);
    game.playNextStep(10);
    expect(game.getCurrentRound()!.getRoundNumber()).toEqual(10);
    expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(20);

    const score = game.getScore().getScoreForGame();
    expect(score[0].summedUpScore).toEqual(20);
    expect(score[1].summedUpScore).toEqual(35);
    expect(score[2].summedUpScore).toEqual(52);
    expect(score[3].summedUpScore).toEqual(61);
    expect(score[4].summedUpScore).toEqual(91);
    expect(score[5].summedUpScore).toEqual(120);
    expect(score[6].summedUpScore).toEqual(139);
    expect(score[7].summedUpScore).toEqual(148);
    expect(score[8].summedUpScore).toEqual(167);
    expect(score[9].summedUpScore).toEqual(187);
    expect(game.isCompleted()).toEqual(true);

    try {
      game.playNextStep();
      fail("Should throw error");
    } catch (e) {
      expect(e).toBeInstanceOf(AllRoundsPlayedError);
    }

    console.log(getCompleteScoreText(game));
  });
});
