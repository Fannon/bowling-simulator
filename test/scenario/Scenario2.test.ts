import { Game } from "../../src/model/Game";
import { getCompleteScoreText } from "../../src/util/textOutput";
import { AllRoundsPlayedError } from "../../src/error/AllRoundsPlayedError";

describe("Scenario 2 (Perfect Game)", () => {
  it("will play successfully", () => {
    const game = new Game();
    // Round 1
    game.playNextStep(10);
    expect(game.getScore().getScoreForGame()[0].summedUpScore).toEqual(10);
    // Round 2
    game.playNextStep(10);
    expect(game.getScore().getScoreForGame()[0].summedUpScore).toEqual(20);
    // Round 3
    game.playNextStep(10);
    expect(game.getScore().getScoreForGame()[0].summedUpScore).toEqual(30);
    // Round 4
    game.playNextStep(10);
    // Round 5
    game.playNextStep(10);
    // Round 6
    game.playNextStep(10);
    // Round 7
    game.playNextStep(10);
    // Round 8
    game.playNextStep(10);
    // Round 9
    game.playNextStep(10);
    // Round 10
    game.playNextStep(10);
    game.playNextStep(10);
    game.playNextStep(10);

    const score = game.getScore().getScoreForGame();
    expect(score[0].summedUpScore).toEqual(30);
    expect(score[1].summedUpScore).toEqual(60);
    expect(score[2].summedUpScore).toEqual(90);
    expect(score[3].summedUpScore).toEqual(120);
    expect(score[4].summedUpScore).toEqual(150);
    expect(score[5].summedUpScore).toEqual(180);
    expect(score[6].summedUpScore).toEqual(210);
    expect(score[7].summedUpScore).toEqual(240);
    expect(score[8].summedUpScore).toEqual(270);
    expect(score[9].summedUpScore).toEqual(300);
    expect(game.isCompleted()).toEqual(true);

    try {
      game.playNextStep();
      fail("Should throw error");
    } catch (e) {
      expect(e).toBeInstanceOf(AllRoundsPlayedError);
    }

    console.log(getCompleteScoreText(game));
  });
});
