import { Game } from "../../src/model/Game";
import {
  getIntroductionText,
  getCompleteScoreText,
  getCurrentRoundInfoText,
  getGameFinnishedText,
} from "../../src/util/textOutput";

describe("util/textOutput", () => {
  describe("getIntroductionText()", () => {
    it("prints introduction text", () => {
      expect(getIntroductionText()).toContain("Welcome to the bowling simulator");
    });
  });

  describe("getCompleteScoreText()", () => {
    it("prints score with empty game", () => {
      const game = new Game();
      expect(getCompleteScoreText(game)).toEqual("");
    });

    it("prints score with empty round", () => {
      const game = new Game();
      game.startNewRound();
      expect(getCompleteScoreText(game)).toContain("ROUND  1");
    });

    it("prints score with played round", () => {
      const game = new Game();
      game.startNewRound();
      game.playNextStep(1);
      game.playNextStep(2);
      const text = getCompleteScoreText(game);
      expect(text).toContain("ROUND  1");
      expect(text).toContain("Score:  3");
      expect(text).toContain("Sum:   3");
      expect(text).toContain("Tosses: [ 1] [ 2]");
    });

    // // Ensures that we don't have an accidental null check
    it("prints scores with 0 values", () => {
      const game = new Game();
      game.startNewRound();
      game.playNextStep(0);
      game.playNextStep(0);
      const text = getCompleteScoreText(game);
      expect(text).toContain("ROUND  1");
      expect(text).toContain("Score:  0");
      expect(text).toContain("Sum:   0");
      expect(text).toContain("Tosses: [ 0] [ 0]");
    });
  });

  describe("getCurrentRoundInfoText()", () => {
    it("pints how many tosses ans pins are left in the current round", () => {
      const game = new Game();
      game.startNewRound();
      const text = getCurrentRoundInfoText(game);
      expect(text).toContain("2 tosses");
      expect(text).toContain("10 pins");
    });
  });

  describe("getGameFinnishedText()", () => {
    it("pints total score", () => {
      const game = new Game();
      game.startNewRound();
      game.playNextStep(5);
      game.playNextStep(5);
      const text = getGameFinnishedText(game);
      expect(text).toContain("10 points");
    });
  });
});
