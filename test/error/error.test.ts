import { GameRuleError } from "../../src/error/GameRuleError";
import { AllRoundsPlayedError } from "../../src/error/AllRoundsPlayedError";
import { AllTossesPlayedError } from "../../src/error/AllTossesPlayedError";

describe("errors", () => {
  describe("AllRoundsPlayedError", () => {
    test("can be constructed", () => {
      const e = new AllRoundsPlayedError();
      expect(e).toBeInstanceOf(GameRuleError);
      expect(e.message).toContain("no round");
    });
  });
  describe("AllTossesPlayedError", () => {
    test("can be constructed", () => {
      const e = new AllTossesPlayedError();
      expect(e).toBeInstanceOf(GameRuleError);
      expect(e.message).toContain("no toss");
    });
    test("can be thrown", () => {
      try {
        throw new AllTossesPlayedError();
      } catch (e) {
        expect(e).toBeInstanceOf(AllTossesPlayedError);
        expect(e.code).toBeDefined();
      }
    });

    test("can be converted to Data", () => {
      const e = new AllTossesPlayedError();
      expect(e.toData().code).toEqual("AllTossesPlayedError");
      expect(e.toData().message).toEqual(e.message);
    });
  });
});
