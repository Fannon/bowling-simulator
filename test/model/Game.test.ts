/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/unbound-method */
import { Game } from "../../src/model/Game";
import { AllRoundsPlayedError } from "../../src/error/AllRoundsPlayedError";
import { Score } from "../../src/model/Score";

describe("model/Game", () => {
  describe("startNewRound()", () => {
    it("can start a new round", () => {
      const game = new Game();
      game.startNewRound();
    });
    it("throws error when trying to start more than 10 rounds", () => {
      const game = new Game();
      const roundOne = game.startNewRound();
      expect(roundOne.getRoundNumber()).toEqual(1);
      game.startNewRound();
      game.startNewRound();
      game.startNewRound();
      game.startNewRound();
      game.startNewRound();
      game.startNewRound();
      game.startNewRound();
      game.startNewRound();
      const roundTen = game.startNewRound();
      expect(roundTen.getRoundNumber()).toEqual(10);
      try {
        game.startNewRound();
        fail("Should throw error");
      } catch (e) {
        expect(e).toBeInstanceOf(AllRoundsPlayedError);
      }
    });
  });

  describe("getCurrentRound()", () => {
    it("gets an undefined round if there wasn't one started yet", () => {
      const game = new Game();
      expect(game.getCurrentRound()).toEqual(undefined);
    });
    it("gets the round that has been startet latest", () => {
      const game = new Game();
      const firstRound = game.startNewRound();
      expect(game.getCurrentRound()).toEqual(firstRound);
      const secondRound = game.startNewRound();
      expect(game.getCurrentRound()).toEqual(secondRound);
      expect(game.getCurrentRound()).not.toEqual(firstRound);
    });
  });

  describe("getRoundsPlayed()", () => {
    it("returns how many rounds have already been played", () => {
      const game = new Game();
      expect(game.getRoundsPlayed()).toEqual(0);
      game.startNewRound();
      expect(game.getRoundsPlayed()).toEqual(1);
    });
  });

  describe("getRoundsLeft()", () => {
    it("returns how many rounds are left to play", () => {
      const game = new Game();
      expect(game.getRoundsLeft()).toEqual(10);
      game.startNewRound();
      expect(game.getRoundsLeft()).toEqual(9);
    });
  });

  describe("isCompleted()", () => {
    it("indicates whether the game is fully completed", () => {
      const game = new Game();
      expect(game.isCompleted()).toEqual(false);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      game.playNextStep(10);
      expect(game.isCompleted()).toEqual(false);
      game.playNextStep(10);
      expect(game.isCompleted()).toEqual(true);
    });
  });

  describe("getScore()", () => {
    it("gets a new score model that calculates the score on the current game state", () => {
      const game = new Game();
      expect(game.getScore()).toBeInstanceOf(Score);
    });
  });
  describe("playNextStep()", () => {
    it("will create a new round if executed on a new game", () => {
      const game = new Game();
      expect(game.getCurrentRound()).toBeUndefined();
      game.playNextStep();
      expect(game.getCurrentRound()).toBeDefined();
    });

    it("will execute a toss in the current round", () => {
      const game = new Game();
      expect(game.getCurrentRound()).toBeUndefined();
      game.playNextStep(1);
      expect(game.getCurrentRound()).toBeDefined();
      expect(game.getCurrentRound()!.getNumberOfPinsBowled()).toEqual(1);
    });

    it("will do a random toss if no argument was provided how many pins are bowled", () => {
      const game = new Game();
      expect(game.getCurrentRound()).toBeUndefined();
      game.playNextStep(0);
      const currentRound = game.getCurrentRound()!;
      currentRound.randomToss = jest.fn();
      game.playNextStep();
      expect(currentRound.randomToss).toHaveBeenCalledTimes(1);
    });

    it("will automatically create a new round when the last one was finnished", () => {
      const game = new Game();
      expect(game.getRoundsPlayed()).toEqual(0);
      game.playNextStep(10);
      const firstRound = game.getCurrentRound()!;
      expect(game.getRoundsPlayed()).toEqual(1);
      game.playNextStep(10);
      const secondRound = game.getCurrentRound()!;
      expect(game.getRoundsPlayed()).toEqual(2);
      expect(firstRound).not.toEqual(secondRound);
    });
  });
});
