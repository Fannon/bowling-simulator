/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { AllTossesPlayedError } from "../../src/error/AllTossesPlayedError";
import { Round } from "../../src/model/Round";
import { InvalidNumberOfPinsBowledError } from "../../src/error/InvalidNumberOfPinsBowledError";

describe("model/Round", () => {
  describe("getRoundNumber()", () => {
    const roundOne = new Round(1);
    expect(roundOne.getRoundNumber()).toEqual(1);
    const roundTen = new Round(10);
    expect(roundTen.getRoundNumber()).toEqual(10);
  });

  describe("toss()", () => {
    it("can toss a specific number of pins", () => {
      const roundOne = new Round(1);
      expect(roundOne["tosses"].length).toEqual(0);
      roundOne.toss(0);
      expect(roundOne["tosses"].length).toEqual(1);
      expect(roundOne["tosses"][0]).toEqual(0);
      roundOne.toss(10);
      expect(roundOne["tosses"].length).toEqual(2);
      expect(roundOne["tosses"][1]).toEqual(10);
    });

    it("throws an error if we try to toss more than twice in a regular round", () => {
      const roundOne = new Round(1);
      roundOne.toss(8);
      roundOne.toss(2);
      try {
        roundOne.toss(3);
        fail("should throw error");
      } catch (e) {
        expect(e).toBeInstanceOf(AllTossesPlayedError);
      }
    });

    it("throws an error if we try to toss more than thrice in the last round", () => {
      const roundTen = new Round(10); // Round 10 has special rules
      roundTen.toss(8);
      roundTen.toss(2);
      // We should be able to toss thrice without getting an error
      roundTen.toss(3);
      try {
        roundTen.toss(4);
        fail("should throw error");
      } catch (e) {
        expect(e).toBeInstanceOf(AllTossesPlayedError);
      }
    });

    it("throws an error if we try to toss thrice in the last round when no strike or spare was achived", () => {
      const roundTen = new Round(10); // Round 10 has special rules
      // We achived a SPARE
      roundTen.toss(5);
      roundTen.toss(2);
      try {
        roundTen.toss(3);
        fail("should throw error");
      } catch (e) {
        expect(e).toBeInstanceOf(AllTossesPlayedError);
      }
    });

    it("throws an error if we try to toss more pins than that are left in this round", () => {
      const roundOne = new Round(1);
      roundOne.toss(8);
      try {
        roundOne.toss(3);
        fail("should throw error");
      } catch (e) {
        expect(e).toBeInstanceOf(InvalidNumberOfPinsBowledError);
        // Error message should indicate that there are only 2 pins left
        expect(e.message).toContain("2");
      }
    });

    it("throws an error if we try to toss less than 0 pins", () => {
      const roundOne = new Round(1);
      try {
        // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
        // @ts-ignore
        roundOne.toss(-3);
        fail("should throw error");
      } catch (e) {
        expect(e).toBeInstanceOf(InvalidNumberOfPinsBowledError);
        // Error message should indicate that there are only 2 pins left
        expect(e.message).toContain("-3");
      }
    });
  });

  describe("getCurrentNumberOfPinsBowled()", () => {
    it("returns how many pins have already been bowled", () => {
      const roundOne = new Round(1);
      expect(roundOne.getNumberOfPinsBowled()).toEqual(0);
      roundOne.toss(1);
      expect(roundOne.getNumberOfPinsBowled()).toEqual(1);
      roundOne.toss(9);
      expect(roundOne.getNumberOfPinsBowled()).toEqual(10);
    });
  });

  describe("getNumberOfPinsBowledInToss()", () => {
    it("returns how many pins have been bowled in a specific toss in a round", () => {
      const roundOne = new Round(1);
      roundOne.toss(1);
      expect(roundOne.getNumberOfPinsBowledInToss(1)).toEqual(1);
      roundOne.toss(9);
      expect(roundOne.getNumberOfPinsBowledInToss(2)).toEqual(9);
      expect(roundOne.getNumberOfPinsBowledInToss(3)).toEqual(undefined);
    });
    it("returns how many pins have been bowled in a specific toss in the last round", () => {
      const roundTen = new Round(10);
      roundTen.toss(6);
      expect(roundTen.getNumberOfPinsBowledInToss(1)).toEqual(6);
      roundTen.toss(4);
      expect(roundTen.getNumberOfPinsBowledInToss(2)).toEqual(4);
      roundTen.toss(10);
      expect(roundTen.getNumberOfPinsBowledInToss(3)).toEqual(10);
      expect(roundTen.getNumberOfPinsBowled()).toEqual(20);
    });
  });

  describe("getNumberOfPinsLeft()", () => {
    it("returns how many pins are left in a regular round", () => {
      const roundOne = new Round(1);
      expect(roundOne.getNumberOfPinsLeft()).toEqual(10);
      roundOne.toss(1);
      expect(roundOne.getNumberOfPinsLeft()).toEqual(9);
      roundOne.toss(9);
      expect(roundOne.getNumberOfPinsLeft()).toEqual(0);
    });
    it("returns how many pins are left in a regular round when a strike was achived ", () => {
      const roundOne = new Round(1);
      expect(roundOne.getNumberOfPinsLeft()).toEqual(10);
      roundOne.toss(10);
      expect(roundOne.getNumberOfPinsLeft()).toEqual(0);
    });
    it("returns how many pins are left in the last round when one strike was achived", () => {
      const roundTen = new Round(10);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(10);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(5);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(5);
      roundTen.toss(5);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(0);
    });
    it("returns how many pins are left in the last round when two strikes were achived", () => {
      const roundTen = new Round(10);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(10);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(10);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(5);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(5);
    });
    it("returns how many pins are left in the last round when a spare was achived", () => {
      const roundTen = new Round(10);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(5);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(5);
      roundTen.toss(5);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(10);
      roundTen.toss(5);
      expect(roundTen.getNumberOfPinsLeft()).toEqual(5);
    });
  });

  describe("getNumberOfPinsBowledInToss()", () => {
    it("returns how many pins have been bowled in a specific toss in the given round", () => {
      const roundOne = new Round(1);
      roundOne.toss(1);
      expect(roundOne.getNumberOfPinsBowledInToss(1)).toEqual(1);
      roundOne.toss(9);
      expect(roundOne.getNumberOfPinsBowledInToss(2)).toEqual(9);
      expect(roundOne.getNumberOfPinsBowledInToss(3)).toEqual(undefined);

      // Also test this for the last round
      const roundTen = new Round(10);
      roundTen.toss(6);
      expect(roundTen.getNumberOfPinsBowledInToss(1)).toEqual(6);
      roundTen.toss(4);
      expect(roundTen.getNumberOfPinsBowledInToss(2)).toEqual(4);
      roundTen.toss(10);
      expect(roundTen.getNumberOfPinsBowledInToss(3)).toEqual(10);
      expect(roundTen.getNumberOfPinsBowled()).toEqual(20);
    });
  });

  describe("getTossesLeft()", () => {
    it("returns how many tosses are left in a regular round", () => {
      const roundOne = new Round(1);
      expect(roundOne.getTossesLeft()).toEqual(2);
      roundOne.toss(0);
      expect(roundOne.getTossesLeft()).toEqual(1);
      roundOne.toss(0);
      expect(roundOne.getTossesLeft()).toEqual(0);
    });
    it("returns how many tosses are left in the last round when no spare or strike was achived", () => {
      const roundTen = new Round(10); // Round 10 has special rules
      expect(roundTen.getTossesLeft()).toEqual(2);
      roundTen.toss(0);
      expect(roundTen.getTossesLeft()).toEqual(1);
      roundTen.toss(0);
      expect(roundTen.getTossesLeft()).toEqual(0);
    });
    it("returns how many tosses are left in the last round, when one strike was achieved", () => {
      const roundTen = new Round(10); // Round 10 has special rules
      expect(roundTen.getTossesLeft()).toEqual(2);
      roundTen.toss(10);
      expect(roundTen.getTossesLeft()).toEqual(2);
      roundTen.toss(5);
      expect(roundTen.getTossesLeft()).toEqual(1);
      roundTen.toss(5);
      expect(roundTen.getTossesLeft()).toEqual(0);
    });
    it("returns how many tosses are left in the last round, when two strike was achieved", () => {
      const roundTen = new Round(10); // Round 10 has special rules
      expect(roundTen.getTossesLeft()).toEqual(2);
      roundTen.toss(10);
      expect(roundTen.getTossesLeft()).toEqual(2);
      roundTen.toss(10);
      expect(roundTen.getTossesLeft()).toEqual(1);
      roundTen.toss(10);
      expect(roundTen.getTossesLeft()).toEqual(0);
    });

    it("returns how many tosses are left in the last round, when a spare was achieved", () => {
      const roundTen = new Round(10); // Round 10 has special rules
      expect(roundTen.getTossesLeft()).toEqual(2);
      roundTen.toss(5);
      expect(roundTen.getTossesLeft()).toEqual(1);
      roundTen.toss(5);
      expect(roundTen.getTossesLeft()).toEqual(1);
      roundTen.toss(10);
      expect(roundTen.getTossesLeft()).toEqual(0);
    });
    it("returns no tosses left when there are no pins left", () => {
      const roundOne = new Round(1);
      expect(roundOne.getTossesLeft()).toEqual(2);
      roundOne.toss(10);
      expect(roundOne.getTossesLeft()).toEqual(0);
    });
  });

  describe("randomToss()", () => {
    it("tosses randomly between 0 and 10 if there are all pins left", () => {
      for (let i = 0; i < 100; i++) {
        const roundOne = new Round(1);
        expect(roundOne.getNumberOfPinsLeft()).toEqual(10);
        roundOne.randomToss();
        expect(roundOne.getNumberOfPinsBowled()).toBeGreaterThanOrEqual(0);
        expect(roundOne.getNumberOfPinsBowled()).toBeLessThanOrEqual(10);
      }
    });
  });
});
