/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Round } from "../../src/model/Round";
import { Score } from "../../src/model/Score";

describe("model/Score", () => {
  describe("getScoreForGame", () => {
    it("returns an array of scores per round", () => {
      const score = new Score([]);
      expect(score.getScoreForGame()).toBeInstanceOf(Array);
      expect(score.getScoreForGame().length).toEqual(0);
    });

    it("calculates the score of a regular round", () => {
      const roundOne = new Round(1);
      roundOne.toss(3);
      roundOne.toss(5);

      const score = new Score([roundOne]);
      expect(score.getScoreForGame()[0]).toBeDefined();
      expect(score.getScoreForGame()[0].roundNumber).toEqual(1);
      expect(score.getScoreForGame()[0].firstToss).toEqual(3);
      expect(score.getScoreForGame()[0].secondToss).toEqual(5);
      expect(score.getScoreForGame()[0].thirdToss).toEqual(undefined);
      expect(score.getScoreForGame()[0].score).toEqual(8);
      expect(score.getScoreForGame()[0].summedUpScore).toEqual(8);
    });

    it("correctly sums up the scores", () => {
      const roundOne = new Round(1);
      roundOne.toss(3);
      roundOne.toss(5);
      const roundTwo = new Round(2);
      roundTwo.toss(2);
      roundTwo.toss(3);

      const score = new Score([roundOne, roundTwo]);
      expect(score.getScoreForGame()[0]).toBeDefined();
      expect(score.getScoreForGame()[0].summedUpScore).toEqual(8);
      expect(score.getScoreForGame()[1].summedUpScore).toEqual(8 + 5);
    });

    it("correctly calculates strike rounds", () => {
      const roundOne = new Round(1);
      roundOne.toss(10);
      const roundTwo = new Round(2);
      roundTwo.toss(2);
      roundTwo.toss(3);

      const score = new Score([roundOne, roundTwo]);
      expect(score.getScoreForGame()[0]).toBeDefined();
      expect(score.getScoreForGame()[0].score).toEqual(10 + 2 + 3);
    });
    it("correctly calculates double strike rounds", () => {
      const roundOne = new Round(1);
      roundOne.toss(10);
      const roundTwo = new Round(2);
      roundTwo.toss(10);
      const roundThree = new Round(3);
      roundThree.toss(10);

      const score = new Score([roundOne, roundTwo, roundThree]);
      expect(score.getScoreForGame()[0]).toBeDefined();
      expect(score.getScoreForGame()[0].score).toEqual(10 + 10 + 10);
    });

    it("correctly calculates spare rounds", () => {
      const roundOne = new Round(1);
      roundOne.toss(3);
      roundOne.toss(7);
      const roundTwo = new Round(2);
      roundTwo.toss(2);
      roundTwo.toss(3);

      const score = new Score([roundOne, roundTwo]);
      expect(score.getScoreForGame()[0]).toBeDefined();
      expect(score.getScoreForGame()[0].score).toEqual(10 + 2);
    });
  });

  describe("getScoreForGame", () => {
    it("calculates the score of a regular round", () => {
      const roundOne = new Round(1);
      roundOne.toss(3);
      roundOne.toss(5);

      const score = new Score([roundOne]);
      expect(score.getScoreForRound(1)).toBeDefined();
      expect(score.getScoreForRound(1)!.roundNumber).toEqual(1);
      expect(score.getScoreForRound(1)!.firstToss).toEqual(3);
      expect(score.getScoreForRound(1)!.secondToss).toEqual(5);
      expect(score.getScoreForRound(1)!.thirdToss).toEqual(undefined);
      expect(score.getScoreForRound(1)!.score).toEqual(8);
      expect(score.getScoreForRound(1)!.summedUpScore).toEqual(8);
    });

    it("returns undefined when the requested round does not exist", () => {
      const score = new Score([]);
      expect(score.getScoreForRound(3)).toEqual(undefined);
    });
  });

  describe("getTotalScore()", () => {
    it("get the total score for an empty game", () => {
      const score = new Score([]);
      expect(score.getTotalScore()).toEqual(0);
    });

    it("get total score of regular rounds", () => {
      const roundOne = new Round(1);
      roundOne.toss(3);
      roundOne.toss(5);
      let score = new Score([roundOne]);
      expect(score.getTotalScore()).toEqual(3 + 5);

      const roundTwo = new Round(2);
      roundTwo.toss(8);
      score = new Score([roundOne, roundTwo]);
      expect(score.getTotalScore()).toEqual(3 + 5 + 8);
    });
  });
});
