import { getRandomIntergerInInclusiveRange } from "../../src/service/randomGenerator";

describe("service/randomGenerator", () => {
  describe("getRandomIntergerInInclusiveRange()", () => {
    it("returns random numbers within an inclusive range", () => {
      for (let i = 0; i < 50; i++) {
        expect(getRandomIntergerInInclusiveRange(0, 10)).toBeGreaterThanOrEqual(0);
        expect(getRandomIntergerInInclusiveRange(0, 10)).toBeLessThanOrEqual(10);
      }
    });
  });
});
