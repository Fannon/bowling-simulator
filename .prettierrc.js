// https://www.robertcooper.me/using-eslint-and-prettier-in-a-typescript-project
module.exports = {
  semi: true,
  trailingComma: "all",
  singleQuote: false,
  quoteProps: "consistent",
  printWidth: 120,
  tabWidth: 2,
};
