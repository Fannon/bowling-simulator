# Bowling Simulator
This is a single-player bowling simulation game.
It is text-based CLI application that works cross-plattform.
The player can either type in how many pins he successfully bowled or let the game choose randomly.

The game will help the player to always play the next logical step in the game. It will also calculate scores.

The game is developed in [https://www.typescriptlang.org/](TypeScript) and runs on a [https://nodejs.org/en/](Node.js) runtime. 

## User Manual
### Prerequisites
* [https://nodejs.org/en/](Node.js) >= 8.9.0; 10 LTS recommended

### Install and build
Since the game is not published in a compiled distribution format on a registry, 
the user has to check out the project and build it locally in order to start it.

This step could be skipped by publishing the game, e.g. to the NPM registry.

```bash
npm install
npm run build
```

### Execute it in the CLI
```bash
npm start
```

## Developer Manual
### Run tests
```bash
npm test
npm run test-watch # For interactive mode
npm run coverage   # to generate coverage reports

npm run lint # to run linting checks
```

### More jobs
* See `package.json` `"scripts"` section
