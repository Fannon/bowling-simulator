/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { RoundNumber } from "./Game";
import { Round, NumberOfPinsRange } from "./Round";

/**
 * Data format that describes the score of one round
 */
export interface ScorePerRound {
  roundNumber: RoundNumber;
  score: number;
  summedUpScore: number;
  firstToss: NumberOfPinsRange | undefined;
  secondToss: NumberOfPinsRange | undefined;
  thirdToss: NumberOfPinsRange | undefined;
}

/**
 * Handles the Game Score rules and calculations
 */
export class Score {
  private rounds: Round[] = [];
  private score: ScorePerRound[] = [];

  constructor(rounds: Round[]) {
    this.rounds = rounds;
    this.score = this.calculateScore();
  }

  /**
   * Get the score for the whole game
   */
  public getScoreForGame(): ScorePerRound[] {
    return this.score;
  }

  /**
   * Get the score for a specific round
   */
  public getScoreForRound(round: RoundNumber): ScorePerRound | undefined {
    return this.score[round - 1];
  }

  /**
   * Get the total score of the game so far
   */
  public getTotalScore(): number {
    if (this.score && this.score.length > 0) {
      return this.score[this.score.length - 1].summedUpScore;
    } else {
      return 0;
    }
  }

  /**
   * Calculates the actual score and returns the results in a data format
   */
  private calculateScore(): ScorePerRound[] {
    const gameScore: ScorePerRound[] = [];

    /** Sum of all calculated scores so far */
    let totalScore = 0;
    for (let i = 0; i < this.rounds.length; i += 1) {
      const currentRound = this.rounds[i];
      const nextRound = this.rounds[i + 1];
      const afterNextRound = this.rounds[i + 2];

      // In any case the score is at least the number of pins bowled in that round
      let score = currentRound.getNumberOfPinsBowled();

      // Additional rules
      if (currentRound.getNumberOfPinsBowledInToss(1) === 10) {
        // RULE: Strike Round was achived, as all pins were bowled in the first toss
        // The bonus is calculated by the number of pins bowled in the next **two** tosses
        if (nextRound) {
          score += nextRound.getNumberOfPinsBowledInToss(1)!;

          if (nextRound.getNumberOfPinsBowledInToss(2) != null) {
            score += nextRound.getNumberOfPinsBowledInToss(2)!;
          } else {
            // If there is no second toss in next round, we need to look for it in the over next
            if (afterNextRound) {
              score += afterNextRound.getNumberOfPinsBowledInToss(1)!;
            }
          }
        }
      } else if (currentRound.getNumberOfPinsBowledInToss(1)! + currentRound.getNumberOfPinsBowledInToss(2)! === 10) {
        // RULE: Spare Round was achived, as all pins were bowled in the first two tosses
        // The bonus is calculated by the number of pins bowled in the next **one** tosses
        if (nextRound) {
          score += nextRound.getNumberOfPinsBowledInToss(1)!;
        }
      }

      totalScore += score;

      gameScore.push({
        roundNumber: currentRound.getRoundNumber(),
        firstToss: currentRound.getNumberOfPinsBowledInToss(1),
        secondToss: currentRound.getNumberOfPinsBowledInToss(2),
        thirdToss: currentRound.getNumberOfPinsBowledInToss(3),
        score: score,
        summedUpScore: totalScore,
      });
    }

    return gameScore;
  }
}
