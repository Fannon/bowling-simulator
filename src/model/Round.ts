import { AllTossesPlayedError } from "../error/AllTossesPlayedError";
import { getRandomIntergerInInclusiveRange } from "../service/randomGenerator";
import { InvalidNumberOfPinsBowledError } from "../error/InvalidNumberOfPinsBowledError";
import { RoundNumber } from "./Game";

/**
 * Number of pins that can be bowled in one toss or be left to toss
 *
 * RULE: There are 10 pins
 */
export type NumberOfPinsRange = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

/** Minimum number of pins that can be bowled in one toss */
export const minNumbersOfPinsBowled: NumberOfPinsRange = 0;

/**
 * Maximum number of pins that can be bowled in one toss
 * EXCEPTION: In the last round, the player is allowed to reset the pins one or two times
 *            which can lead to up to 30 pins
 */
export const numberOfPins: NumberOfPinsRange = 10;

/**
 * Amount of tosses available in one round
 * Note that in the last round, we can gain an additinal toss under certain conditions
 */
export const tossesPerRound = 2;

export const lastRoundNumber = 10;

/**
 * Number of tosses the player has left in this round
 */
export type TossesLeft = 0 | 1 | 2;

/**
 * Represents a round in the bowling game
 */
export class Round {
  /** Number of this round */
  private roundNumber: RoundNumber;

  private tosses: NumberOfPinsRange[] = [];

  constructor(roundNumber: RoundNumber) {
    this.roundNumber = roundNumber;
  }

  /**
   * Get the number of the current round
   */
  public getRoundNumber(): RoundNumber {
    return this.roundNumber;
  }

  /**
   * Get number of pins which have been bowled in this round already
   */
  public getNumberOfPinsBowled(): number {
    let pinsBowled = 0;
    for (const toss of this.tosses) {
      pinsBowled += toss;
    }
    return pinsBowled;
  }

  /**
   * Get the number of pins that have been bowled in a specific toss in this round
   */
  public getNumberOfPinsBowledInToss(tossNumber: 1 | 2 | 3): NumberOfPinsRange | undefined {
    return this.tosses[tossNumber - 1];
  }

  /**
   * Get number of pins that are left in this round
   *
   * RULE: In the last round a player may have an additonal third toss
   *       if he achived a strike or spare within the current round
   *       In this case, the number of pins left will be reseted
   */
  public getNumberOfPinsLeft(): NumberOfPinsRange {
    // For the last round, we need to increase the amount of pins available within the round
    // as we can reset them once or twice
    let totalPins = numberOfPins;
    if (this.roundNumber === lastRoundNumber) {
      if (this.tosses[0] === numberOfPins) {
        // If the player achieved a strike in the first round, we reset the pins
        totalPins += numberOfPins;
      }
      if (this.tosses[0] === numberOfPins && this.tosses[1] === numberOfPins) {
        // If the player achieved a strike in the first AND the second round, we reset the pins
        totalPins += numberOfPins;
      }
      if (this.tosses[0] + this.tosses[1] === numberOfPins) {
        // If the player achieved a spare, we reset the pins
        totalPins += numberOfPins;
      }
    }
    return (totalPins - this.getNumberOfPinsBowled()) as NumberOfPinsRange;
  }

  /**
   * Get how many tosses the player has left in this round
   * RULE: In the last round a player may have an additional third toss,
   *       if he achieved a strike or spare within that round.
   */
  public getTossesLeft(): TossesLeft {
    let bonusTosses = 0;

    if (this.roundNumber === lastRoundNumber) {
      if (this.tosses[0] === numberOfPins) {
        // RULE: Add bonus toss for strike in last round
        bonusTosses += 1;
      } else if (this.tosses[0] + this.tosses[1] === numberOfPins) {
        // RULE: Add bonus toss for spare in last round
        bonusTosses += 1;
      }
    }

    if (this.getNumberOfPinsLeft() === 0) {
      // RULE: If there are no pins left to bowl, we dont need to have another toss
      return 0;
    } else {
      return (tossesPerRound - this.tosses.length + bonusTosses) as TossesLeft;
    }
  }

  /**
   * Do a new toss in this round
   * Ensures that the player does not toss more often than he is allowed to do
   *
   * @throws AllTossesPlayedError
   * @throws InvalidNumberOfPinsBowledError
   */
  public toss(numberOfPinsBowled: NumberOfPinsRange): void {
    // Ensure that the player doesn't toss more often than allowed
    if (this.getTossesLeft() === 0) {
      throw new AllTossesPlayedError();
    }

    // We can only bowl so much pins which are still left
    if (numberOfPinsBowled > this.getNumberOfPinsLeft()) {
      throw new InvalidNumberOfPinsBowledError(numberOfPinsBowled, this.getNumberOfPinsLeft());
    }

    // We also can't bowl less than 0 pins
    if (numberOfPinsBowled < 0) {
      throw new InvalidNumberOfPinsBowledError(numberOfPinsBowled, this.getNumberOfPinsLeft());
    }

    this.tosses.push(numberOfPinsBowled);
  }

  /**
   * Bowl a random number of pins
   *
   * If we do random bowls, we should never try to bowl more pins than there are currently left over
   */
  public randomToss(): void {
    const numberOfPinsBowled = getRandomIntergerInInclusiveRange(
      minNumbersOfPinsBowled,
      this.getNumberOfPinsLeft(),
    ) as NumberOfPinsRange;
    this.toss(numberOfPinsBowled);
  }
}
