import { AllRoundsPlayedError } from "../error/AllRoundsPlayedError";
import { Round, NumberOfPinsRange } from "./Round";
import { Score } from "./Score";

/**
 * Number rounds that a game can have
 *
 * RULE: There are 10 rounds
 */
export type RoundNumber = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;

/**
 * Maximum number of rounds that can be played
 */
export const maxNumberOfRounds: RoundNumber = 10;

/**
 * The actual Game class, which is the main model of this application
 */
export class Game {
  private playedRounds: Round[] = [];

  /**
   * How many rounds have already been played
   */
  public getRoundsPlayed(): RoundNumber {
    return this.playedRounds.length as RoundNumber;
  }

  /**
   * How many rounds are left to play
   */
  public getRoundsLeft(): RoundNumber | 0 {
    return (maxNumberOfRounds - this.getRoundsPlayed()) as RoundNumber;
  }

  /**
   * Whether the game has finnished or not
   *
   * It's advised to use this function to detect when a player should call `playNextStep`
   * otherwise it will lead to GameRuleViolation Errors
   */
  public isCompleted(): boolean {
    const currentRound = this.getCurrentRound();
    return this.getRoundsLeft() === 0 && currentRound != null && currentRound.getTossesLeft() === 0;
  }

  public startNewRound(): Round {
    if (this.playedRounds.length === 10) {
      throw new AllRoundsPlayedError();
    }
    const newRoundNumber = (this.playedRounds.length + 1) as RoundNumber;
    const newRound = new Round(newRoundNumber);
    this.playedRounds.push(newRound);
    return newRound;
  }

  public getCurrentRound(): Round | undefined {
    if (this.playedRounds.length > 0) {
      return this.playedRounds[this.playedRounds.length - 1];
    } else {
      return undefined;
    }
  }

  /**
   * Play the next logical step
   * This will coordinate the game instances methods according to the game rules,
   * e.g. it will start the game or new rounds if they haven't been started yet
   *
   * @param numberOfPinsBowled if the param is not provided, a random value will be chosen
   */
  public playNextStep(numberOfPinsBowled?: NumberOfPinsRange): void {
    let currentRound = this.getCurrentRound();

    if (!currentRound) {
      currentRound = this.startNewRound();
    }

    if (currentRound.getTossesLeft() === 0) {
      currentRound = this.startNewRound();
    }

    if (numberOfPinsBowled != null) {
      currentRound.toss(numberOfPinsBowled);
    } else {
      currentRound.randomToss();
    }
  }

  public getScore(): Score {
    return new Score(this.playedRounds);
  }
}
