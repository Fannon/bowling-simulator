/**
 * This utility module is used for converting the model and its resulting state / data
 * into text output, which will be used by the CLI entry point
 */

import { Game } from "../model/Game";

export function getIntroductionText(): string {
  let text = "";
  text += "==============================================================\n";
  text += "Welcome to the bowling simulator!\n";
  text += "==============================================================\n";
  text += "Press ENTER to toss\n";
  text += "  Provide no input for a random toss\n";
  text += "  Provide a number from 0 to 10 to toss a specific number of pins\n";
  text += "Press CTRL+C to exit the game\n";
  text += "--------------------------------------------------------------\n";
  return text;
}

/**
 * Return the score as a text that can be outputted to the console
 */
export function getCompleteScoreText(game: Game): string {
  let text = "";
  const scorePerRound = game.getScore().getScoreForGame();
  for (const score of scorePerRound) {
    text += `ROUND ${score.roundNumber.toString().padStart(2)}    `;
    text += `Score: ${score.score.toString().padStart(2)}      `;
    text += `Sum: ${score.summedUpScore.toString().padStart(3)}      `;
    text += "Tosses: ";
    if (score.firstToss != null) {
      text += `[${score.firstToss.toString().padStart(2)}]`;
    }
    if (score.secondToss != null) {
      text += ` [${score.secondToss.toString().padStart(2)}]`;
    }
    if (score.thirdToss != null) {
      text += ` [${score.thirdToss.toString().padStart(2)}]`;
    }
    text += "\n";
  }
  return text;
}

/**
 * Returns information about the current round
 * e.g. how many tosses and pins are left
 */
export function getCurrentRoundInfoText(game: Game): string {
  let text = "";
  const currentRound = game.getCurrentRound();
  if (currentRound) {
    const pinsLeft = currentRound.getNumberOfPinsLeft();
    const tossesLeft = currentRound.getTossesLeft();
    text += `You have ${tossesLeft} tosses and ${pinsLeft} pins left in this round\n`;
    text += "\n";
  } else {
    text += "Error: There is no current Round in the Game\n";
  }
  text += "--------------------------------------------------------------\n";
  return text;
}

export function getGameFinnishedText(game: Game): string {
  let text = "";
  const totalScore = game.getScore().getTotalScore();
  text += "\n";
  text += "==============================================================\n";
  text += "CONGRATULATIONS! You have finnished the game!\n";
  text += `You have scored ${totalScore} points in total\n`;
  text += "You can continue with a new game\n";
  text += "==============================================================\n";
  text += "\n";
  return text;
}
