import { BowlingSimulatorError } from "./BowlingSimulatorError";

/**
 * Abstract error class that all game rule related errors must inherit from
 */
export abstract class GameRuleError extends BowlingSimulatorError {}
