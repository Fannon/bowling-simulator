import { GameRuleError } from "./GameRuleError";

export class AllTossesPlayedError extends GameRuleError {
  public code = "AllTossesPlayedError";
  constructor() {
    super("In this round no toss is possible anymore");
  }
}
