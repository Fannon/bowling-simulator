/* eslint-disable @typescript-eslint/no-explicit-any */

export interface ErrorData {
  code: string;
  message: string;
  [key: string]: any;
}

/**
 * Abstract error class that all Bowling Simulator related errors must inherit from
 */
export abstract class BowlingSimulatorError extends Error {
  abstract code: string;

  public toString(): string {
    let errorText = `Error: ${this.message}`;
    const errorJson = JSON.stringify(this, getCircularReplacer(), 2);
    if (errorJson != "{}") {
      errorText += "\n" + errorJson;
    }
    if (this.stack) {
      errorText += "\n" + this.stack.replace(`Error: ${this.message}`, "").trim();
    }
    return errorText;
  }

  public toData(): ErrorData {
    const errorData = JSON.parse(JSON.stringify(this, getCircularReplacer()));
    errorData.message = this.message;
    return errorData;
  }
}

/**
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value
 */
export function getCircularReplacer(): any {
  const seen = new WeakSet();
  return (key: any, value: any): any => {
    if (typeof value === "object" && value !== null) {
      if (seen.has(value)) {
        return;
      }
      seen.add(value);
    }
    return value;
  };
}
