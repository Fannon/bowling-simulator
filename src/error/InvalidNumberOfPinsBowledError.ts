import { NumberOfPinsRange } from "../model/Round";
import { GameRuleError } from "./GameRuleError";

export class InvalidNumberOfPinsBowledError extends GameRuleError {
  public code = "InvalidNumberOfPinsBowledError";
  constructor(pinsBowled: number, maxAllowed: NumberOfPinsRange) {
    super(`The player tried to bowl ${pinsBowled} pins, but there are only ${maxAllowed} pins left`);
  }
}
