import { GameRuleError } from "./GameRuleError";

export class AllRoundsPlayedError extends GameRuleError {
  public code = "AllRoundsPlayedError";
  constructor() {
    super("In this game no round is possible anymore");
  }
}
