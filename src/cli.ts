/**
 * This file is used as the entry point for CLI usage and will be executed on `npm start`
 * This is just one possible entrypoint/consumer which could use the actual game logic
 */

import * as readline from "readline";
import { GameRuleError } from "./error/GameRuleError";
import { Game } from "./model/Game";
import { NumberOfPinsRange } from "./model/Round";
import * as textOutput from "./util/textOutput";

const rl = readline.createInterface({ input: process.stdin, output: process.stdout });

console.log(textOutput.getIntroductionText());

export let game = new Game();

/**
 * Promts the user how many pins he wants to bowl
 *
 * If the user enters nothing, `undefined` will be returned in the callbackFn
 * If the user enters a number it will be returned (range will not be checked, as this is game logic)
 * If the user enters something that cannot be parsed to a number, an error will be displayed
 * and the user can try again
 *
 * @param callbackFn Callback function that is called each time the users hits enter
 */
export function promtForNumberOfPins(callbackFn: (numberOfPins?: NumberOfPinsRange) => void): void {
  rl.on("line", (input: string) => {
    if (input.trim() !== "") {
      const pinsBowled = parseInt(input.trim(), 10) as NumberOfPinsRange;
      if (!isNaN(pinsBowled)) {
        callbackFn(pinsBowled);
      } else {
        console.error(`Received invalid input: "${input}"`);
      }
    } else {
      callbackFn(undefined);
    }
  });
}

promtForNumberOfPins(numberOfPins => {
  try {
    game.playNextStep(numberOfPins);
    console.log(textOutput.getCompleteScoreText(game));
    console.log(textOutput.getCurrentRoundInfoText(game));

    if (game.isCompleted()) {
      console.log(textOutput.getGameFinnishedText(game));
      game = new Game();
    }
  } catch (e) {
    if (e instanceof GameRuleError) {
      console.error(`Game Rule Violation: ${e.message}`);
    } else {
      console.error(e);
    }
  }
});
